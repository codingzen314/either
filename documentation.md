# Either

An Either data structure is made up of two generic types *L* (left) and *R* (right) and is represented by the type `Either<L, R>`.  An instance of an 
Either holds an instance of *L* or an instance of *R* but NOT an instance of BOTH.

<p align="center">
    <img src="./readme-resources/figure1.0.jpg">
</p>
<p align="center">
    <span>Figure 1.0: visual representations of an Either&lt;L, R&gt;</span>
</p>

## Why use an Either

Either is most often used as a practical approach to deal with the complexities of error handling.  The general idea is to represent errors using *L* and
 the computed value using *R*.  Originally a function would look like
```kotlin
fun findById(id: Int): Person? = ...
```
However, now our functions will look like  
```kotlin
fun findById(id: Int): Either<AppError, Person?> = ...
```

<p align="center">
    <img src="./readme-resources/findById.jpg">
</p>
<p align="center">
    <span style=""display:block>Figure 1.0: findById visual representation</span>
</p>

For `findById` the happy path value is `Person?` and error path value is `AppError`.  [^1] Here is a possible implementation of `findById`
```
fun findById(id: Int): Either<AppError, Person?> = 
    Either
        .catching { personRepository.findById(id) }
        .mapLeft { exception -> //construct instance of AppError here }
```
*Note*: Most (probably all) sql libraries do not use `Either` so we're using `Either.catching` to wrap the repository call inside a *try-catch* where 
the exception will be stored in the left and retrieved Person entity will be stored in the right. 

This approach to error handling is both extensible and scalable and here's a small glimpse of those properties
<p align="center">
    <img src="./readme-resources/genericHttpRequestResponse.jpg"/>
</p>
<p align="center">
    <span>Figure 1.1: http request/response workflow</span>
</p>

## Creation
Before we get to composition we first need to know how to construct Either instances:
- Left construction 
    ```kotlin
    val either: Either<String, Nothing> = Either.left("left value")
    ```
- Right construction 
    ```kotlin
    val either: Either<Nothing, Int> = Either.right(34)
    ```

*Note*: Please remember that the *L* (left) and *R* (right) sides DO NOT have to be the same type

## Composition
We'll use a small part of *Figure 1.1* to help us understand what we want composition to mean
<p align="center">
    <img src="./readme-resources/figure1.2.jpg"/>
</p>
<p align="center">
    <span>Figure 1.2: reference for composition</span>
</p>

In *Figure 1.2* *validate* produces *ev* and *update_db* produces *eu*.  There must be some way that *update_db* is receiving an instanceof *S* (comes from 
*ev* right side) or *update_db* is bypassed if *validate* fails (comes from *ev* left side)  Let's take a look at *update_db* in isolation knowing that is
has access to *ev* 
<p align="center">
    <img src="./readme-resources/figure1.3.jpg"/>
</p>
<p align="center">
    <span>Figure 1.3: update db internals</span>
</p>

We can see that the transformation `F: (S) -> T` is bypassed if *ev* is a left.  Otherwise *F* is applied to *ev*'s right value to get an instance of *T*.  We now
have enough information to formulate a rough draft definition:

For any `Either<E, S>` we can compose that Either using a transformation function `F: (S) -> T` to get a resultant `Either<E, T>`

#### Bind

There is ambiguity in the rough draft definition!  What happens if F can fail?  We know that buying into Either means we can't just let the exception be 
thrown (that is just lazy and we have `Either.catching`).  

Updating the transformation *F* to allow failure gives a new type signature `F: (S) -> Either<E, T>` and results in the following figure
<p align="center">
    <img src="./readme-resources/figure1.4.jpg"/>
</p>
<p align="center">
    <span>Figure 1.4: bind in a picture</span>
</p>

Here's an implementation of our definition of Composition [^2]
```kotlin
inline fun <L, R, S> Either<L, R>.bind(fn: (R) -> Either<L, S>): 
  Either<L, S> =
  when (this) {
    is Left -> this
    is Right -> fn(right)
  }
```

Understanding *Figure 1.4* and *bind*'s type signature let's revisit what *update db* (*Figure 1.2*) actually is.  *update db* is the transformation
function `F: (S) -> Either<E, T>` (*fn* argument to *bind*) applied via *bind* to the result of *validation*

#### Map

Let's take a function `f: (Person) -> String = { p: Person -> p.name }` and use that with *bind*.  That looks like 
```kotlin
val ep: Either<AppError, Person> = ...
val result: Either<AppError, String> = ep.bind { p: Person -> Either.right(f(p)) }
```

Which works just fine but looks cumbersome due the to wrapping of `f(p)` with `Either.right`.  For these kinds of functions that we know 
will never fail there is another composition function for Either, named *map*
```kotlin
inline fun <L, R, S> Either<L, R>.map(fn: (R) -> S): Either<L, S> = 
    bind { Either.right(f(it)) }
``` 

This method is what *Figure 1.3* illustrates.  


## Footnotes
  

[^1]: You may be asking "Wait, this function can still throw an Exception?"  You are correct because all functions/methods
on the jvm can throw Exceptions.  However, buying into Either requires discipline (all good software development requires discipline)
and if you don't trust the author (third-party library) you can always wrap that call with `Either.catching`.  A goal of this library is to make the
discipline simpler via syntax and type inference. 

[^2]: *bind* and *flatMap* are the most historically used names of the function.  This library chooses *bind* because it is shorter