package tech.codingzen

/**
 * @param L left type
 * @param R right type
 *
 * Representation of a data structure that holds one value.  That value will be an instance of type L or type R
 */
sealed class Either<out L, out R> {
  companion object {
    /**
     * @param L left type
     * @param [l] left value
     * @return Either containing the left value [l]
     */
    fun <L> left(l: L): Either<L, Nothing> = Left(l)

    /**
     * @param R right type
     * @param [r] right value
     * @return Either containing the right value [r]
     */
    fun <R> right(r: R): Either<Nothing, R> = Right(r)

    /**
     * @param R right type
     * @param [fn] used to produce a value.  This function may throw an Exception
     * @return Either containing an Exception thrown during the application of [fn] or the right value that is the
     * result of applying [fn]
     */
    inline fun <R> catching(fn: () -> R): Either<Throwable, R> =
      fixedLeftSyntax<Throwable>().run {
        try {
          right(fn())
        } catch (e: Throwable) {
          left(e)
        }
      }

    /**
     * @param I input type
     * @param R right type
     * @param [fn] used to produce a value.  This function may throw an Exception
     * @return a function that produces an Either containing an Exception thrown during the application of [fn] or the
     * right value that is the result of applying [fn]
     */
    inline fun <I, R> catchingFn(crossinline fn: (I) -> R): (I) -> Either<Throwable, R> =
      fixedLeftSyntax<Throwable>().run {
        { input: I ->
          try {
            right(fn(input))
          } catch (e: Throwable) {
            left(e)
          }
        }
      }

    @JvmStatic
    /**
     * cached instance of EitherFixedLeftSyntax
     */
    private val fixedLeftSyntaxImpl = object : EitherFixedLeftSyntax<Any> {}

    @JvmStatic
    /**
     * cached instance of EitherFixedRightSyntax
     */
    private val fixedRightSyntaxImpl = object : EitherFixedRightSyntax<Any> {}

    /**
     * @param L left type
     * @return instance of EitherFixedLeftSyntax
     */
    fun <L> fixedLeftSyntax(): EitherFixedLeftSyntax<L> =
      @Suppress("UNCHECKED_CAST") (fixedLeftSyntaxImpl as EitherFixedLeftSyntax<L>)

    /**
     * @param R right type
     * @return instance of EitherFixedLeftSyntax
     */
    fun <R> fixedRightSyntax(): EitherFixedRightSyntax<R> =
      @Suppress("UNCHECKED_CAST") (fixedRightSyntaxImpl as EitherFixedRightSyntax<R>)
  }
}

/**
 * Represents the left side of an Either.  Instances of this type contain the value of the left side
 *
 * @param L left type
 * @property left value of this Left
 * @constructor creates a Left side containing [left]
 */
data class Left<out L> internal constructor(val left: L) : Either<L, Nothing>()

/**
 * Represents the right side of an Either.  Instances of this type contain the value of the right side
 *
 * @param R right type
 * @property right value of this Right
 * @constructor creates a Right side containing [right]
 */
data class Right<out R> internal constructor(val right: R) : Either<Nothing, R>()

/**
 * @param L left type
 *
 * Syntax class used as a receiver type for lambda expressions used in most Either fun's.  Instances of this interface
 * have a fixed left type but variable right type
 */
interface EitherFixedLeftSyntax<L> {
  /**
   * @param R right type
   * @param [right] value of the right side of the produced Either
   * @return a Right containing [right]
   */
  fun <R> right(right: R): Either<L, R> = Right(right)

  /**
   * @param [left] value of the left side of the produced Either
   * @return a Left containing [left]
   */
  fun left(left: L): Either<L, Nothing> = Left(left)
}

/**
 * @param R right type
 *
 * Syntax class used as a receiver type for lambda expressions used in most Either fun's.  Instances of this interface
 * have a fixed right type but variable left type
 */
interface EitherFixedRightSyntax<R> {
  /**
   * @param L left type
   * @param [left] value of the left side of the produced Either
   * @return a Left containing [left]
   */
  fun <L> left(left: L): Either<L, R> = Left(left)

  /**
   * @param [right] value of the right side of the produced Either
   * @return a Right containing [right]
   */
  fun right(right: R): Either<Nothing, R> = Right(right)
}

/**
 * @receiver Either to be fold
 * @param L left type
 * @param R right type
 * @param A result type
 * @param [fnL] function to apply to the left value of the receiver
 * @param [fnR] function to apply to the right value of the receiver
 * @return the result of applying [fnL] or [fnR] to this Either
 */
inline fun <L, R, A> Either<L, R>.fold(fnL: (L) -> A, fnR: (R) -> A): A =
  when (this) {
    is Left -> fnL(left)
    is Right -> fnR(right)
  }

/**
 * A builder style class for a fluent api when fold'ing an Either
 *
 * @param L left type
 * @param R right type
 * @param A result type
 * @property either the either that will be fold'ed
 * @property fnL left fn for the fold
 */
class FoldIntermediate<L, R, A>(val either: Either<L, R>, val fnL: (L) -> A)

/**
 * @receiver FoldIntermediate to specify the right fn on
 * @param L left type
 * @param R right type
 * @param A result type
 * @param [fnR] right fn for the fold on this FoldIntermediate
 * @return the result of fold'ing the Either referenced by [this]
 */
fun <L, R, A> FoldIntermediate<L, R, A>.foldR(fnR: (R) -> A): A = this.either.fold(fnL, fnR)

/**
 * @receiver Either to fold
 * @param L left type
 * @param R right type
 * @param A result type
 * @param [fnL] left fn for the fold on this Either
 * @return the result of applying [fnL] or fnR to this Either where [fnL] and fnR are defined by this method
 * and [FoldIntermediate]
 */
fun <L, R, A> Either<L, R>.foldL(fnL: (L) -> A): FoldIntermediate<L, R, A> = FoldIntermediate(this, fnL)

/**
 * @receiver Either to check if it is a left
 * @return `true` if this Either is a left, `false` otherwise
 */
fun Either<*, *>.isLeft(): Boolean = fold({ true }, { false })

/**
 * @receiver Either to check if it is a right
 * @return `true` if this Either is a right, `false` otherwise
 */
fun Either<*, *>.isRight(): Boolean = fold({ false }, { true })

/**
 * @receiver Either to bind
 * @param L left type
 * @param R right type
 * @param S new right type
 * @param [fn] fn used to transform the right value of this Either
 * @return the result of applying [fn] to the right value of this Either.  If this Either is a left it is returned
 * unchanged
 */
@Suppress("UNCHECKED_CAST")
inline fun <L, R, S> Either<L, R>.bindSyntax(fn: EitherFixedLeftSyntax<L>.(R) -> Either<L, S>): Either<L, S> =
  fold({ this as Either<L, S> }, { Either.fixedLeftSyntax<L>().fn(it) })

/**
 * @receiver Either to bind
 * @param L left type
 * @param R right type
 * @param S new right type
 * @param [fn] fn used to transform the right value of this Either
 * @return the result of applying [fn] to the right value of this Either.  If this Either is a left it is returned
 * unchanged
 */
@Suppress("UNCHECKED_CAST")
inline fun <L, R, S> Either<L, R>.bind(fn: (R) -> Either<L, S>): Either<L, S> =
  fold({ this as Either<L, S> }, fn)

/**
 * @receiver Either to map
 * @param L left type
 * @param R right type
 * @param S new right type
 * @param [fn] fn used to transform the right value of this Either
 * @return an Either containing a right value that is the result of applying [fn] to the right value of this Either.  If
 * this Either is a left it is returned unchanged
 */
@Suppress("UNCHECKED_CAST")
inline fun <L, R, S> Either<L, R>.map(fn: (R) -> S): Either<L, S> = bindSyntax { right(fn(it)) }

/**
 * @param L left type
 * @param R right type
 *
 * Used to support a vertical syntax for the filter operation via
 * ```
 * e.filter {
 *   test = { ... }
 *   handler = { ... }
 * }
 * ```
 */
class FilterSyntax<L, R> {
  /**
   * test function for the filter
   */
  lateinit var test: (R) -> Boolean
  /**
   * handler function to handle test failures
   */
  lateinit var handler: (R) -> Either<L, R>

  /**
   * @return the [test] function
   */
  operator fun component1(): (R) -> Boolean = test

  /**
   * @return the [handler] function
   */
  operator fun component2(): (R) -> Either<L, R> = handler
}

/**
 * @receiver Either to filter
 * @param L left type
 * @param R right type
 * @param [body] used to facilitate a more vertical syntax for the filter operation
 * @return this Either if it passed the filter test or contained a left value, otherwise the result of applying
 * [handler] (defined by [body]) to the right value that did not pass the filter test
 */
inline fun <L, R> Either<L, R>.filter(body: FilterSyntax<L, R>.() -> Unit): Either<L, R> =
  this.bind { r ->
    FilterSyntax<L, R>().apply(body).let { (test, handler) -> if (test(r)) this@filter else handler(r) }
  }

/**
 * @receiver this Either
 * @param L left type
 * @param R right type
 * @param [body] block of code that is executed if this Either contains a right value
 * @return this Either unchanged
 */
inline fun <L, R> Either<L, R>.peekRight(body: (R) -> Unit): Either<L, R> =
  this.apply {
    when (this) {
      is Right -> body(right)
    }
  }

/**
 * @receiver this Either
 * @param L left type
 * @param R right type
 * @param [body] block of code that is executed if this Either contains a left value
 * @return this Either unchanged
 */
inline fun <L, R> Either<L, R>.peekLeft(body: (L) -> Unit): Either<L, R> =
  this.apply {
    when (this) {
      is Left -> body(left)
    }
  }

/**
 * @receiver this Either
 * @param L left type
 * @param R right type
 * @param [block] block of code that is executed if this Either contains a left value
 * @return right value of this Either
 */
inline fun <L, R> Either<L, R>.onLeft(block: (Left<L>) -> Nothing): R =
  when (this) {
    is Left -> block(this)
    is Right -> right
  }

/**
 * @receiver this Either
 * @param L left type
 * @param R right type
 * @param [block] block of code that is executed if this Either contains a left value
 * @return right value of this Either
 */
inline fun <L, R> Either<L, R>.onRight(block: (Right<R>) -> Nothing): L =
  when (this) {
    is Left -> left
    is Right -> block(this)
  }

/**
 * @receiver this Either
 * @param T both left and right type
 * @return the left or right value of this Either
 */
fun <T> Either<T, T>.get(): T =
  when (this) {
    is Left -> left
    is Right -> right
  }

/**
 * @receiver this Either to get the left value out of
 * @param L left type
 * @return the left value of this Either, otherwise throws [NoSuchElementException]
 * @throws [NoSuchElementException] if this Either contains a right value
 */
val <L> Either<L, *>.left: L get() = this.fold({ it }, { throw NoSuchElementException("this Either contains a right value!") })

/**
 * @receiver this Either to get the right value out of
 * @param R right type
 * @return the right value of this Either, otherwise throws [NoSuchElementException]
 * @throws [NoSuchElementException] if this Either contains a left value
 */
val <R> Either<*, R>.right: R get() = this.fold({ throw NoSuchElementException("this Either contains a right value!") }, { it })

/**
 * @receiver this Either to get the right value out of
 * @param R right type
 * @param [default] value to return if this Either contains a left value
 * @return this Either's right value or [default] if this Either contains a left value
 */
fun <R> Either<*, R>.orElse(default: R): R = this.fold({ default }, { it })

/**
 * @receiver this Either to get the right value out of
 * @param L left type
 * @param R right type
 * @param [fn] mapping function to transform the left value to an instance of [R]
 * @return this Either's right value or the value of applying [fn] to this Either's left value
 */
inline fun <L, R> Either<L, R>.orElseMap(fn: (L) -> R): R = this.fold(fn, { it })

/**
 * @receiver this Either to get the right value out of
 * @param L left type
 * @param R right type
 * @param [fn] function that produces a default value
 * @return this Either's right value or the value of applying [fn]
 */
inline fun <L, R> Either<L, R>.orElseGet(fn: () -> R): R = this.fold({ fn() }, { it })

/**
 * @receiver this Either to get the right value out of
 * @param L left type
 * @param R right type
 * @param [fn] function that produces the [Throwable] to throw
 * @return this Either's right value or throws the exception produced by [fn]
 * @throws Exception produced by [fn]
 */
inline fun <R> Either<*, R>.orThrow(fn: () -> Throwable): R = this.fold({ throw fn() }, { it })

/**
 * @receiver Either to bind
 * @param L left type
 * @param R right type
 * @param M new left type
 * @param [fn] fn used to transform the left value of this Either
 * @return the result of applying [fn] to the left value of this Either.  If this Either is a right it is returned
 * unchanged
 */
@Suppress("UNCHECKED_CAST")
inline fun <L, R, M> Either<L, R>.bindLeftSyntax(fn: EitherFixedRightSyntax<R>.(L) -> Either<M, R>): Either<M, R> =
  fold({ Either.fixedRightSyntax<R>().fn(it) }, { this as Either<M, R> })

/**
 * @receiver Either to bind
 * @param L left type
 * @param R right type
 * @param M new left type
 * @param [fn] fn used to transform the left value of this Either
 * @return the result of applying [fn] to the left value of this Either.  If this Either is a right it is returned
 * unchanged
 */
@Suppress("UNCHECKED_CAST")
inline fun <L, R, M> Either<L, R>.bindLeft(fn: (L) -> Either<M, R>): Either<M, R> =
  fold(fn, { this as Either<M, R> })

/**
 * @receiver Either to map
 * @param L left type
 * @param R right type
 * @param M new right type
 * @param [fn] fn used to transform the left value of this Either
 * @return an Either containing a left value that is the result of applying [fn] to the left value of this Either.  If
 * this Either is a right it is returned unchanged
 */
@Suppress("UNCHECKED_CAST")
inline fun <L, R, M> Either<L, R>.mapLeft(fn: (L) -> M): Either<M, R> = bindLeftSyntax { left(fn(it)) }

/**
 * @param L left type
 * @param R right type
 *
 * Used to support a vertical syntax for the filter operation via
 * ```
 * e.filter {
 *   test = { ... }
 *   handler = { ... }
 * }
 * ```
 */
class FilterLeftSyntax<L, R> {
  /**
   * test function for the filter
   */
  lateinit var test: (L) -> Boolean
  /**
   * handler function to handle test failures
   */
  lateinit var handler: (L) -> Either<L, R>

  /**
   * @return the [test] function
   */
  operator fun component1(): (L) -> Boolean = test

  /**
   * @return the [handler] function
   */
  operator fun component2(): (L) -> Either<L, R> = handler
}

/**
 * @receiver Either to filterLeft
 * @param L left type
 * @param R right type
 * @param [body] used to facilitate a more vertical syntax for the filterLeft operation
 * @return this Either if it passed the filter test or contained a right value, otherwise the result of applying
 * [handler] (defined by [body]) to the left value that did not pass the filter test
 */
inline fun <L, R> Either<L, R>.filterLeft(body: FilterLeftSyntax<L, R>.() -> Unit): Either<L, R> =
  this.bindLeft { l ->
    FilterLeftSyntax<L, R>().apply(body).let { (test, handler) -> if (test(l)) this@filterLeft else handler(l) }
  }

/**
 * @receiver this Either to join
 * @param L left type
 * @param R right type
 * @return this Either join'ed to result in an un-nested Either type
 */
@Suppress("UNCHECKED_CAST")
fun <L, R> Either<L, Either<L, R>>.join(): Either<L, R> = this
  .foldL { this@join as Either<L, R> }
  .foldR { it }

/**
 * @receiver this Either to join
 * @param L left type
 * @param R right type
 * @return this Either join'ed to result in an un-nested Either type
 */
@JvmName("joinViaLeft")
@Suppress("UNCHECKED_CAST")
fun <L, R> Either<Either<L, R>, R>.join(): Either<L, R> = this
  .foldL { it }
  .foldR { this@join as Either<L, R> }

/**
 * @receiver Either to bindTry
 * @param L left type
 * @param R right type
 * @param S new right type
 * @param [fn] fn used to transform the right value of this Either.  This function has a receiver type of
 * [EitherFixedLeftSyntax] to provide access to the [EitherFixedLeftSyntax.left] and [EitherFixedLeftSyntax.right]
 * @return the result of applying [fn] to the right value of this Either.  If this Either is a left it is returned
 * unchanged.  If during the application of [fn] an exception is thrown then [handler] (defined by body) is used
 * to convert the right value and exception into an Either
 */
inline fun <L, R, S> Either<L, R>.bindTry(fn: EitherFixedLeftSyntax<L>.(R) -> Either<L, S>): EitherTryResult<L, R, S> =
  when (this) {
    is Left -> EitherTryResult.EitherResult(this)
    is Right ->
      try {
        val x = Either.fixedLeftSyntax<L>().fn(right)
        EitherTryResult.EitherResult(x)
      } catch (e: Throwable) {
        EitherTryResult.ExceptionThrown(right, e)
      }
  }

/**
 * @receiver Either to mapTry
 * @param L left type
 * @param R right type
 * @param S new right type
 * @param [fn] fn used to transform the right value of this Either
 * @return the result of applying [fn] to the right value of this Either.  If this Either is a left it is returned
 * unchanged.  If during the application of [fn] an exception is thrown then [handler] (defined by body) is used
 * to convert the right value and exception into an Either
 */
inline fun <L, R, S> Either<L, R>.mapTry(fn: (R) -> S): EitherTryResult<L, R, S> = bindTry { right(fn(it)) }

/**
 * @param L left type
 * @param R right type
 * @param S new right type
 * Used to hold an Either or a right value and Exception.
 */
sealed class EitherTryResult<out L, out R, out S> {
  /**
   * @param L left type
   * @param new right type
   * @property e source Either
   */
  data class EitherResult<out L, out S>(val e: Either<L, S>): EitherTryResult<L, Nothing, S>()

  /**
   * @param R right type
   * @property r right value
   * @property exc exception
   */
  data class ExceptionThrown<R>(val r: R, val exc: Throwable): EitherTryResult<Nothing, R, Nothing>()
}

/**
 * @receiver EitherTryResult to handle
 * @param L left type
 * @param R right type
 * @param S new right type
 * @param [fn] lambda with receiver type of [EitherFixedLeftSyntax], used to transform the right value and exception into an Either
 * @return the result of applying [fn] if this EitherTryResult contains a right value and exception.  If this EitherTryResult
 * contains an Either then that Either is returned unchanged
 */
inline fun <L, R, S> EitherTryResult<L, R, S>.handler(fn: EitherFixedLeftSyntax<L>.(R, Throwable) -> Either<L, S>): Either<L, S> =
  when (this) {
    is EitherTryResult.EitherResult -> e
    is EitherTryResult.ExceptionThrown -> Either.fixedLeftSyntax<L>().fn(r, exc)
  }

/**
 * @receiver List containing one or more Either
 * @param L left type
 * @param R right type
 * @return an Either whose right holds a list of ALL right values in the receiver, or an Either whose left holds the
 * first left encountered when sequenc'ing the receiver
 */
fun <L, R> List<Either<L, R>>.sequence(): Either<L, List<R>> {
  when {
    this.isEmpty() -> return Either.right(listOf())
    this.size == 1 -> return this[0].map { listOf(it) }
    else -> {
      val list = mutableListOf<R>()
      for (either in this) {
        if (either.isLeft()) return either as Either<L, List<R>>
        else list.add(either.right)
      }
      return Either.right(list.toList())
    }
  }
}