package tech.codingzen

import assertk.Assert
import assertk.assertions.support.fail

fun Assert<Either<*, *>>.isLeft(): Unit = given { either ->
  if (either.isRight()) assertk.fail(message = "Expected a Left but was a Right")
}

fun <L, R> Assert<Either<L, R>>.isLeft(expected: L): Unit = given { either ->
  when (either) {
    is Left -> if (either.left != expected) fail(expected, either.left)
    is Right -> assertk.fail(expected = expected, message = "Expected a Left but was a Right")
  }
}

fun Assert<Either<*, *>>.isRight(): Unit = given { either ->
  if (either.isLeft()) assertk.fail(message = "Expected a Right but was a Left")
}

fun <L, R> Assert<Either<L, R>>.isRight(expected: R): Unit = given { either ->
  when (either) {
    is Left -> assertk.fail(expected = expected, message = "Expected a Right but was a Left")
    is Right -> if (either.right != expected) fail(expected, either.right)
  }
}