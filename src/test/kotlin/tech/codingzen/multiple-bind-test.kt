package tech.codingzen

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.Test


class MultipleBindTest {
    val good: Either<String, Int> = Either.right(10)
    val fail: Either<String, Int> = Either.left("fail")
    val corr: Either<String, String> = Either.right("correct")

    @Test
    fun tuple2() {
        assertThat(bindAll(good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail) { "correct" }).isEqualTo(fail)
    }

    @Test
    fun tuple3() {
        assertThat(bindAll(good, good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, fail) { "correct" }).isEqualTo(fail)
    }

    @Test
    fun tuple4() {
        assertThat(bindAll(good, good, good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, fail) { "correct" }).isEqualTo(fail)
    }

    @Test
    fun tuple5() {
        assertThat(bindAll(good, good, good, good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, fail, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, fail) { "correct" }).isEqualTo(fail)
    }

    @Test
    fun tuple6() {
        assertThat(bindAll(good, good, good, good, good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, fail, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, fail, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, good, fail) { "correct" }).isEqualTo(fail)
    }

    @Test
    fun tuple7() {
        assertThat(bindAll(good, good, good, good, good, good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good, good, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail, good, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, fail, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, fail, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, fail, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, good, fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, good, good, fail) { "correct" }).isEqualTo(fail)
    }

    @Test
    fun tuple8() {
        assertThat(bindAll(good, good, good, good, good, good, good, good) { "correct" }).isEqualTo(corr)
        assertThat(bindAll(fail, good, good, good, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, fail, good, good, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, fail, good, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, fail, good, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, fail, good, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, good, fail, good, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, good, good, fail, good) { "correct" }).isEqualTo(fail)
        assertThat(bindAll(good, good, good, good, good, good, good, fail) { "correct" }).isEqualTo(fail)
    }

}