package tech.codingzen

import assertk.assertThat
import assertk.assertions.*
import org.junit.Test
import tech.codingzen.Either.Companion.left
import tech.codingzen.Either.Companion.right
import java.util.*
import kotlin.NoSuchElementException

class EitherTests {
  @Test
  fun fold_isLeft() {
    assertThat(left("foo").fold({ 101 }, { 9 })).isEqualTo(101)
  }

  @Test
  fun fold_isRight() {
    assertThat(right("foo").fold({ 101 }, { 9 })).isEqualTo(9)
  }

  @Test
  fun fold_verticalSyntax_isLeft() {
    val r = left("foo")
      .foldL { 101 }
      .foldR { 9 }
    assertThat(r).isEqualTo(101)
  }

  @Test
  fun fold_verticalSyntax_isRight() {
    val r = right("foo")
      .foldL { 101 }
      .foldR { 9 }
    assertThat(r).isEqualTo(9)
  }

  @Test
  fun isLeft_isLeft() {
    assertThat(left(10).isLeft()).isTrue()
  }

  @Test
  fun isLeft_isRight() {
    assertThat(right(10).isLeft()).isFalse()
  }

  @Test
  fun isRight_isLeft() {
    assertThat(right(10).isLeft()).isFalse()
  }

  @Test
  fun isRight_isRight() {
    assertThat(right(10).isRight()).isTrue()
  }

  @Test
  fun bind_isLeft() {
    assertThat(left(100).bind { right(9) }.isLeft()).isTrue()

    val e: Either<Int, String> = left(100)
    val e1 = e.bind { left(90) }
    assertThat(e1.isLeft()).isTrue()
  }

  @Test
  fun bind_isRight() {
    val e: Either<String, Int> = right(100)
    assertThat(e.bind { left("") }.isLeft()).isTrue()
    assertThat(right(100).bind { right(it - 15) }).isEqualTo(right(85))
  }

  @Test
  fun map_isLeft() {
    assertThat(left(100).map { "" }.isLeft()).isTrue()
  }

  @Test
  fun map_isRight() {
    assertThat(right("foo").map(String::length)).isEqualTo(right(3))
  }

  @Test
  fun filter_isLeft() {
    val e: Either<String, Int> = left("foo")
    val expected: Either<String, Int> = left("crap")
    assertThat(e
      .filter {
        test = { it == 100 }
        handler = { expected }
      }
      .isLeft()).isTrue()
  }

  @Test
  fun filter_failed_isRight() {
    val e: Either<String, Int> = right(9)
    val expected: Either<String, Int> = left("crap")
    assertThat(e
      .filter {
        test = { it == 100 }
        handler = { expected }
      }).isEqualTo(expected)
  }

  @Test
  fun filter_passed_isRight() {
    val e: Either<String, Int> = right(9)
    assertThat(e
      .filter {
        test = { it == 9 }
        handler = { left("crap") }
      }).isEqualTo(e)
  }

  @Test
  fun onRight_isLeft() {
    var store: Int? = null
    val e = left(100)
    assertThat(e.peekRight { store = 3 }).isEqualTo(e)
    assertThat(store).isNull()
  }

  @Test
  fun onRight_iRight() {
    var store: Int? = null
    val e = right(100)
    assertThat(e.peekRight { store = it }).isEqualTo(e)
    assertThat(store).isEqualTo(100)
  }

  @Test
  fun onLeft_isLeft() {
    var store: Int? = null
    val e = left(100)
    assertThat(e.peekLeft { store = it }).isEqualTo(e)
    assertThat(store).isEqualTo(100)
  }

  @Test
  fun onLeft_iRight() {
    var store: Int? = null
    val e = right(100)
    assertThat(e.peekLeft { store = 3 }).isEqualTo(e)
    assertThat(store).isNull()
  }

  @Test
  fun right_isLeft() {
    assertThat { left(100).right }.isFailure().isInstanceOf(NoSuchElementException::class.java)
  }

  @Test
  fun right_isRight() {
    assertThat(right("foo").right).isEqualTo("foo")
  }

  @Test
  fun left_isLeft() {
    assertThat(left("foo").left).isEqualTo("foo")
  }

  @Test
  fun left_isRight() {
    assertThat { left(100).right }.isFailure().isInstanceOf(NoSuchElementException::class.java)
  }

  @Test
  fun orElse_isLeft() {
    assertThat(left(123).orElse("foo")).isEqualTo("foo")
  }

  @Test
  fun orElse_isRight() {
    assertThat(right("foo").orElse("bar")).isEqualTo("foo")
  }

  @Test
  fun orElseMap_isLeft() {
    assertThat(left(123).orElseMap { "bar" }).isEqualTo("bar")
  }

  @Test
  fun orElseMap_isRight() {
    assertThat(right("foo").orElseMap { "bar" }).isEqualTo("foo")
  }

  @Test
  fun orElseGet_isLeft() {
    assertThat(left(123).orElseGet { "bar" }).isEqualTo("bar")
  }

  @Test
  fun orElseGet_isRight() {
    assertThat(right("foo").orElseGet { "bar" }).isEqualTo("foo")
  }

  @Test
  fun orThrow_isLeft() {
    assertThat { left(100).orThrow { IllegalStateException("should be thrown") } }.isFailure().isInstanceOf(IllegalStateException::class.java)
  }

  @Test
  fun orThrow_isRight() {
    assertThat(right(100).orThrow { IllegalStateException("should be thrown") }).isEqualTo(100)
  }

  @Test
  fun bindLeft_isRight() {
    assertThat(right(100).bindLeft { right(9) }.isRight()).isTrue()
  }

  @Test
  fun bindLeft_isLeft() {
    val e: Either<Int, String> = left(100)
    assertThat(e.bindLeft { right("") }.isRight()).isTrue()
    assertThat(left(100).bindLeft { left(it - 15) }).isEqualTo(left(85))
  }

  @Test
  fun mapLeft_isRight() {
    assertThat(right(100).mapLeft { "" }.isRight()).isTrue()
  }

  @Test
  fun mapLeft_isLeft() {
    assertThat(left("foo").mapLeft(String::length)).isEqualTo(left(3))
  }

  @Test
  fun filterLeft_isRight() {
    val e: Either<Int, String> = right("foo")
    val expected: Either<Int, String> = left(11)
    assertThat(e
      .filterLeft {
        test = { it == 100 }
        handler = { expected }
      }
      .isRight()).isTrue()
  }

  @Test
  fun filterLeft_failed_isLeft() {
    val e: Either<Int, String> = left(9)
    val expected: Either<Int, String> = left(10)
    assertThat(e
      .filterLeft {
        test = { it == 100 }
        handler = { expected }
      }).isEqualTo(expected)
  }

  @Test
  fun filterLeft_passed_isLeft() {
    val e: Either<Int, String> = left(100)
    assertThat(e
      .filterLeft {
        test = { it == 100 }
        handler = { right("not this one") }
      }).isEqualTo(e)
  }

  @Test
  fun join_viaRight() {
    val e0: Either<String, Either<String, Int>> = left("foo")
    assertThat(e0.join()).isEqualTo(left("foo"))

    val e1: Either<String, Either<String, Int>> = right(left("foo"))
    assertThat(e1.join()).isEqualTo(left("foo"))
  }

  @Test
  fun join_viaLeft() {
    val e0: Either<Either<String, Int>, String> = right("foo")
    assertThat(e0.join()).isEqualTo(right("foo"))

    val e1: Either<Either<String, Int>, String> = left(left("foo"))
    assertThat(e1.join()).isEqualTo(left("foo"))
  }

  @Test
  fun bindTry_isLeft() {
    val e: Either<Char, String> = left('f')
    val e0 = e
      .bindTry { if (it == "foo") left('r') else right(34) }
      .handler { _, _ -> left('3') }
    assertThat(e0).isEqualTo(e)
  }

  @Test
  fun bindTry_isRight_throws() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .bindTry { right(UUID.fromString("lkasjf")) }
      .handler { _, _ -> left('3') }
    assertThat(e0).isEqualTo(left('3'))
  }

  @Test
  fun bindTry_isRight() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .bindTry { right(Integer.parseInt("34")) }
      .handler { _, _ -> left('3') }
    assertThat(e0).isEqualTo(right(34))
  }

  @Test
  fun mapTry_isLeft() {
    val e: Either<Char, String> = left('f')
    val e0 = e
      .mapTry { Integer.parseInt("2") }
      .handler { _, _ -> right(50) }
    assertThat(e0).isEqualTo(e)
  }

  @Test
  fun mapTry_isRight_throwsException() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .mapTry { Integer.parseInt("fr") }
      .handler { _, _ -> right(50) }
    assertThat(e0).isEqualTo(right(50))
  }

  @Test
  fun mapTry_isRight() {
    val e: Either<Char, String> = right("foo")
    val e0 = e
      .mapTry { Integer.parseInt("56") }
      .handler { _, _ -> left('k') }
    assertThat(e0).isEqualTo(right(56))
  }

  @Test
  fun catching_throwException() {
    val e = Either.catching { Integer.parseInt("afj") }
    assertThat(e.isLeft()).isTrue()
    assertThat(e.left).isInstanceOf(NumberFormatException::class.java)
  }

  @Test
  fun catching_value() {
    val e = Either.catching { Integer.parseInt("65") }
    assertThat(e).isEqualTo(right(65))
  }

  @Test
  fun catchingFn_throwException() {
    val f = Either.catchingFn { s: String -> Integer.parseInt(s) }
    val e = f("foj")
    assertThat(e.isLeft()).isTrue()
    assertThat(e.left is NumberFormatException).isTrue()
  }

  @Test
  fun catchingFn_value() {
    val f = Either.catchingFn { s: String -> Integer.parseInt(s) }
    val e = f("65")
    assertThat(e).isEqualTo(right(65))
  }

  @Test
  fun sequence_empty() {
    val e = listOf<Either<String, Int>>().sequence()
    assertThat(e.isRight()).isTrue()
    assertThat(e.right.isEmpty()).isTrue()
  }

  @Test
  fun sequence_single_success() {
    val e = listOf(Either.right("foo")).sequence()
    assertThat(e.isRight()).isTrue()
    assertThat(e.right[0]).isEqualTo("foo")
  }

  @Test
  fun sequence_single_error() {
    val e = listOf(Either.left("oops")).sequence()
    assertThat(e.isLeft()).isTrue()
    assertThat(e.left).isEqualTo("oops")
  }


  @Test
  fun sequence_list_success() {
    val e = listOf(
      Either.right("foo"),
      Either.right("bar"),
      Either.right("baz"))
      .sequence()
    assertThat(e.isRight()).isTrue()
    assertThat(e.right.size).isEqualTo(3)
    assertThat(e.right).isEqualTo(listOf("foo", "bar", "baz"))
  }

  @Test
  fun sequence_list_last_err() {
    val e = listOf(
      Either.right("foo"),
      Either.right("bar"),
      Either.left("oops"))
      .sequence()
    assertThat(e.isLeft()).isTrue()
    assertThat(e.left).isEqualTo("oops")
  }

  @Test
  fun get() {
    assertThat((Either.left(12) as Either<Int, Int>).get()).isEqualTo(12)
    assertThat((Either.right(12) as Either<Int, Int>).get()).isEqualTo(12)
  }

  @Test
  fun onLeft() {
    val f = fn@ { e: Either<String, Int> ->
      val x: Int = e.onLeft { return@fn it }
      right(x + 1)
    }

    val l = left("yep")
    assertThat(f(l)).isEqualTo(l)
    assertThat(f(right(2))).isEqualTo(right(3))
  }

  @Test
  fun onRight() {
    val f = fn@ { e: Either<String, Int> ->
      val x: String = e.onRight { return@fn it }
      left(x.toUpperCase())
    }

    val r = right(1)
    assertThat(f(r)).isEqualTo(r)
    assertThat(f(left("abc"))).isEqualTo(left("ABC"))
  }
}